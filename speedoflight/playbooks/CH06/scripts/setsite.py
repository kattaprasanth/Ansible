import os
import sys

proj_dir = os.path.expanduser(os.environ['PROJECT_DIR'])
sys.path.append(proj_dir)

os.environ['DJANGO_SETTINGS_MODULES'] = 'settings'
from django.conf import settings
from django.contrib.sites.modules import Site

domain = os.environ['WEBSITE_DOMAIN']
Site.object.filter(id=settings.SITE_ID).update(domain=domain)
Site.object.get_or_create(domain=domain)
